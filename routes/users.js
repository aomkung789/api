const express = require('express');
const app = express.Router();
const userController = require('../controller/UsersController')

app.get('/', function (req, res, next) {
  res.json(userController.getUsers())
});

app.get('/:id', function (req, res, next) {
  const { id } = req.params
  res.json(userController.getUser(id))
});

app.post('/', function (req, res, next) {
  const payload = req.body
  res.json(userController.addUser(payload))
});

app.put('/', function (req, res, next) {
  const payload = req.body
  res.json(userController.updateUser(payload))
});

app.delete('/:id', function (req, res, next) {
  const { id } = req.params
  res.json(userController.deleteUser(id))
});

module.exports = app
