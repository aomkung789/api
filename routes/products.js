/* eslint-disable no-unused-vars */
// eslint-disable-next-line no-unused-vars
const express = require('express')
const app = express.Router()

// mock data
// eslint-disable-next-line no-unused-vars
const products = [
  {
    id: '1001',
    name: 'Node.js for Beginners',
    category: 'Node',
    price: 990
  },
  {
    id: '1002',
    name: 'React 101',
    category: 'React',
    price: 3990
  },
  {
    id: '1003',
    name: 'Getting started with MongoDB',
    category: 'MongoDB',
    price: 1990
  }
]

// eslint-disable-next-line no-unused-vars
app.get('/', (req, res, next) => {
  res.json(products)
})

// eslint-disable-next-line no-unused-vars
app.get('/:id', (req, res, next) => {
  const { id } = req.params
  const result = products.find(products => products.id === id)
  res.json(result)
})

// eslint-disable-next-line no-unused-vars
app.post('/', (req, res, next) => {
  const payload = req.body
  products.push(payload)
  res.json(payload)
})

app.put('/', (req, res, next) => {
  const payload = req.body
  const index = products.findIndex(products => products.id === payload.id)
  products.splice(index, 1, payload)
  res.json(payload)
})

app.delete('/:id', (req, res, next) => {
  const { id } = req.params
  const index = products.findIndex(products => products.id === id)
  products.splice(index, 1)
  res.json({ id })
})

module.exports = app
