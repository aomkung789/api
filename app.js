var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')

var indexRouter = require('./routes/index')
// eslint-disable-next-line no-unused-vars
var usersRouter = require('./routes/users')
// eslint-disable-next-line no-unused-vars
var helloRouter = require('./routes/hello')

var productsRouter = require('./routes/products')
var app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/users', usersRouter)
app.use('/', indexRouter)
app.use('/hello', helloRouter)
app.use('/products', productsRouter)
module.exports = app
